﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LazyBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void M_Maximize_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        private void M_User_Settings(object sender, RoutedEventArgs e)
        {
            GridMainPage.Children.Clear();
            GridMainPage.Children.Add(new UserSettingsControl());
        }

        private void M_Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void M_Minimize_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
